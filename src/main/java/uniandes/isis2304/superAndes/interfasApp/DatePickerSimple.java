package uniandes.isis2304.superAndes.interfasApp;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DatePickerSimple extends Application {

    Date fecha;
    static java.sql.Date fechaTest;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Elegir Fecha");

        DatePicker datePicker = new DatePicker();


        datePicker.setOnAction(action->{
            LocalDate value = datePicker.getValue();
            System.out.println(value);
            Date date = Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant());

            System.out.println(date);
            fecha=date;
            fechaTest=new java.sql.Date(date.getTime());
            primaryStage.close();
        });

        HBox hbox = new HBox(datePicker);


        Scene scene = new Scene(hbox, 300, 240);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args) {
        Application.launch();
        System.out.println("Fecha: "+fechaTest);
    }

    public java.sql.Date getFecha()
    {
        Application.launch();
        return fechaTest;
    }
}