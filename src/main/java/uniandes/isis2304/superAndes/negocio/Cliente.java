package uniandes.isis2304.superAndes.negocio;


/**
 * Clase que representa al cliente
 * se modela con los datos del cliente
 * los clientes asisten a un supermercado en donde compran
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:11 p. m.
 */
public class Cliente  {
	private long ClienteID;
	private String Email;
	private String Nombre;

	public Cliente(long clienteID, String email, String nombre) {
		ClienteID = clienteID;
		Email = email;
		Nombre = nombre;
	}

	public Cliente()
	{

	}

	public long getClienteID() {
		return ClienteID;
	}

	public void setClienteID(long clienteID) {
		ClienteID = clienteID;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}
}//end Cliente