package uniandes.isis2304.superAndes.negocio;


import oracle.sql.DATE;

/**
 * Clase que describe el producto del proveedor
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:24 p. m.
 */
public class ProductoProeveedor extends Producto {

	private long ProductoproeveedorID;
	private float Precioventasupermercado;

	public ProductoProeveedor() {
	}

	public ProductoProeveedor( long productoproeveedorID, float precioventasupermercado) {
		ProductoproeveedorID = productoproeveedorID;
		Precioventasupermercado = precioventasupermercado;
	}

	public long getProductoproeveedorID() {
		return ProductoproeveedorID;
	}

	public void setProductoproeveedorID(long productoproeveedorID) {
		ProductoproeveedorID = productoproeveedorID;
	}

	public float getPrecioventasupermercado() {
		return Precioventasupermercado;
	}

	public void setPrecioventasupermercado(float precioventasupermercado) {
		Precioventasupermercado = precioventasupermercado;
	}
}
