package uniandes.isis2304.superAndes.negocio;

import java.util.*;
/**
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:14 p. m.
 */
public class SuperAndes  {

	private long SuperandesID;

	private String Nombre;
	public  SuperAndes()
	{

	}

	public long getSuperandesID() {
		return SuperandesID;
	}

	public void setSuperandesID(long superandesID) {
		SuperandesID = superandesID;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public SuperAndes(long superandesID, String nombre) {
		SuperandesID = superandesID;
		Nombre = nombre;
	}
}//end SuperAndes