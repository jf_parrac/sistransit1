package uniandes.isis2304.superAndes.negocio;


import java.sql.Date;

/**
 * Clase que describe al proveedor
 * Elementos del servicio que provee
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:21 p. m.
 */
public class Proveedor  {

    private long ProveedorID;

    private String Calificaciodecalidad;

    private Date Fechaentraga;

    private String Nit;

    private String Nombre;

    private long SuperandesID;
    public Proveedor()
    {

    }

    public Proveedor(long proveedorID, String calificaciodecalidad, Date fechaentrega, String nit, String nombre, long superandesID) {
        ProveedorID = proveedorID;
        Calificaciodecalidad = calificaciodecalidad;
        Fechaentraga = fechaentrega;
        Nit = nit;
        Nombre = nombre;
        SuperandesID = superandesID;
    }

    public long getProveedorID() {
        return ProveedorID;
    }

    public void setProveedorID(long proveedorID) {
        ProveedorID = proveedorID;
    }

    public String getCalificaciodecalidad() {
        return Calificaciodecalidad;
    }

    public void setCalificaciodecalidad(String calificaciodecalidad) {
        Calificaciodecalidad = calificaciodecalidad;
    }

    public Date getFechaentraga() {
        return Fechaentraga;
    }

    public void setFechaentraga(Date fechaentraga) {
        Fechaentraga = fechaentraga;
    }

    public String getNit() {
        return Nit;
    }

    public void setNit(String nit) {
        Nit = nit;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public long getSuperandesID() {
        return SuperandesID;
    }

    public void setSuperandesID(long superandesID) {
        SuperandesID = superandesID;
    }
}//end Proeveedor