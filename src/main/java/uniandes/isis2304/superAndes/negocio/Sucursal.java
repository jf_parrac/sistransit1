package uniandes.isis2304.superAndes.negocio;


import java.util.ArrayList;

/**
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:15 p. m.
 */
public class Sucursal extends SuperAndes  {
	private long SucursalID;

	private String Ciudad;

	private String Direccion;
	private String Segmernomercado;

	private long SuperMercado;

	public long getSucursalID() {
		return SucursalID;
	}

	public void setSucursalID(long sucursalID) {
		SucursalID = sucursalID;
	}

	public String getCiudad() {
		return Ciudad;
	}
	public Sucursal()
	{

	}

	public Sucursal(long sucursalID, String ciudad, String direccion, String segmernomercado, long superMercado) {
		SucursalID = sucursalID;
		Ciudad = ciudad;
		Direccion = direccion;
		Segmernomercado = segmernomercado;
		SuperMercado = superMercado;
	}

	public void setCiudad(String ciudad) {
		Ciudad = ciudad;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public String getSegmernomercado() {
		return Segmernomercado;
	}

	public void setSegmernomercado(String segmernomercado) {
		Segmernomercado = segmernomercado;
	}

	public long getSuperMercado() {
		return SuperMercado;
	}

	public void setSuperMercado(long superMercado) {
		SuperMercado = superMercado;
	}
}//end Sucursal