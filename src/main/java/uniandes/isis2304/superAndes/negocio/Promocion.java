package uniandes.isis2304.superAndes.negocio;

import java.sql.Date;
import java.util.ArrayList;

public class Promocion  {

    private long PROMOCIONID;

    private long PRODUCTOPROMOCION;

    private int TIPOPROMOCION;

    private String DESCRIPCIONPROMOCION;

    private Date FECHAINICIO;

    private Date FECHAFIN;

    public Promocion()
    {

    }

    public Promocion(long PROMOCIONID, long PRODUCTOPROMOCION, int TIPOPROMOCION, String DESCRIPCIONPROMOCION, Date FECHAINICIO, Date FECHAFIN) {
        this.PROMOCIONID = PROMOCIONID;
        this.PRODUCTOPROMOCION = PRODUCTOPROMOCION;
        this.TIPOPROMOCION = TIPOPROMOCION;
        this.DESCRIPCIONPROMOCION = DESCRIPCIONPROMOCION;
        this.FECHAINICIO = FECHAINICIO;
        this.FECHAFIN = FECHAFIN;
    }

    public long getPROMOCIONID() {
        return PROMOCIONID;
    }

    public void setPROMOCIONID(long PROMOCIONID) {
        this.PROMOCIONID = PROMOCIONID;
    }

    public long getPRODUCTOPROMOCION() {
        return PRODUCTOPROMOCION;
    }

    public void setPRODUCTOPROMOCION(long PRODUCTOPROMOCION) {
        this.PRODUCTOPROMOCION = PRODUCTOPROMOCION;
    }

    public int getTIPOPROMOCION() {
        return TIPOPROMOCION;
    }

    public void setTIPOPROMOCION(int TIPOPROMOCION) {
        this.TIPOPROMOCION = TIPOPROMOCION;
    }

    public String getDESCRIPCIONPROMOCION() {
        return DESCRIPCIONPROMOCION;
    }

    public void setDESCRIPCIONPROMOCION(String DESCRIPCIONPROMOCION) {
        this.DESCRIPCIONPROMOCION = DESCRIPCIONPROMOCION;
    }

    public Date getFECHAINICIO() {
        return FECHAINICIO;
    }

    public void setFECHAINICIO(Date FECHAINICIO) {
        this.FECHAINICIO = FECHAINICIO;
    }

    public Date getFECHAFIN() {
        return FECHAFIN;
    }

    public void setFECHAFIN(Date FECHAFIN) {
        this.FECHAFIN = FECHAFIN;
    }
}
