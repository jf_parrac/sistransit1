package uniandes.isis2304.superAndes.negocio;


/**
 * Tipos de productos
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:21 p. m.
 */
public enum eCategoria {
	PERECEDEROS,
	NO_PERECEDEROS,
	ASEO,
	CONGELADOS,
	ROPA,
	MUEBLES,
	HERRAMIENTAS,
	ELETRODOMESTICOS
	,OTROS
}