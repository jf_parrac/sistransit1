package uniandes.isis2304.superAndes.negocio;


/**
 * Clase que describe el estante
 * Los productos de la bodega se colocan en el estante 
 * tiene un tipo y un nivel de abastecimiento especifico
 * ubicado en una sucursal
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:18 p. m.
 */
public class Estante  {

	private long EstanteID;
	private float Nivelavastecimiento;
	private String Tipoestante;
	private long SucursalID;
	public Estante()
	{

	}

	public Estante(long estanteID, float nivelacastecimiento, String tipoestante, long sucursalID) {
		EstanteID = estanteID;
		Nivelavastecimiento = nivelacastecimiento;
		Tipoestante = tipoestante;
		SucursalID = sucursalID;
	}

	public long getEstanteID() {
		return EstanteID;
	}

	public void setEstanteID(long estanteID) {
		EstanteID = estanteID;
	}

	public float getNivelavastecimiento() {
		return Nivelavastecimiento;
	}

	public void setNivelavastecimiento(float nivelavastecimiento) {
		Nivelavastecimiento = nivelavastecimiento;
	}

	public String getTipoestante() {
		return Tipoestante;
	}

	public void setTipoestante(String tipoestante) {
		Tipoestante = tipoestante;
	}

	public long getSucursalID() {
		return SucursalID;
	}

	public void setSucursalID(long sucursalID) {
		SucursalID = sucursalID;
	}
}//end Estante