package uniandes.isis2304.superAndes.negocio;

/**
 * describe una persona natural
 * tiene un documendo de identificacion
 * @author User
 * @version 1.0
 * @created 29-sept.-2018 6:50:09 p. m.
 */
public class PersonaNatural extends Cliente  {

	private long ID;
	private String Documentoidentificaion;

	public PersonaNatural()
	{

	}
	public PersonaNatural(long ID,String documentoidentificaion)
	{
		this.ID=ID;
		this.Documentoidentificaion=documentoidentificaion;
	}
	public long getID() {
		return ID;
	}

	public void setID(long ID) {
		this.ID = ID;
	}

	public String getDocumentoidentificaion() {
		return Documentoidentificaion;
	}

	public void setDocumentoidentificaion(String documentoidentificaion) {
		Documentoidentificaion = documentoidentificaion;
	}
}//end uniandes.isis2304.superAndes.negocio.Persona Natural