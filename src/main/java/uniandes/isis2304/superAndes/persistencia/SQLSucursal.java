package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.sql.Date;

public class SQLSucursal {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;
    public SQLSucursal(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }


    public long adicionarSucursal(PersistenceManager pm, long SucursalID, String Ciudad, String Direccion, String segmentomercado)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaSucursal() + "\" (\"SucursalID\",\"Ciudad\",\"Direccion\",\"Segmenromercado\") values (?, ?, ? ,? )");
        q.setParameters(SucursalID,Ciudad,Direccion,segmentomercado);
        return (long) q.executeUnique();
    }
    public long eliminarProveedorr (PersistenceManager pm, long SucursalID)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaSucursal() + "\" WHERE \"SucursalID\" = ?");
        q.setParameters(SucursalID);
        return (long) q.executeUnique();
    }
}
