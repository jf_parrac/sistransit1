package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.sql.Date;

public class SQLClienteSuperAndes {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLClienteSuperAndes(PersistenciaSuperAndes pp) {
        this.pp = pp;
    }

    public long adicionarCliente(PersistenceManager pm, long supermercado, long cliente) {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaClienteSuperAndes()+ "\"(\"supermercado\",\"cliente\") values (?, ?)");
        q.setParameters(supermercado,cliente);
        return (long) q.executeUnique();
    }

    public long eliminarCliente(PersistenceManager pm, long supermercado,long cliente) {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaClienteSuperAndes() + "\" WHERE \"supermercado\" = ? AND \"cliente\"= ?");
        q.setParameters(supermercado,cliente);
        return (long) q.executeUnique();
    }
}
