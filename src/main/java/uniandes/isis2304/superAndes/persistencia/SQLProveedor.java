package uniandes.isis2304.superAndes.persistencia;

import uniandes.isis2304.superAndes.negocio.Proveedor;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.Date;
import java.util.List;

public class SQLProveedor {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;
    public SQLProveedor(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarProveedor(PersistenceManager pm, long ProveedorID, Date Fechaentraga, String Nit, String Nombre, long SuperAndesID)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaProveedor() + "\"(\"ProveedorID\",\"Nit\",\"Nombre\",\"SuperandesID\",\"Fechaentraga\") values (?, ? ,? ,?,?)");
        q.setParameters(ProveedorID,Nit,Nombre,SuperAndesID,new java.sql.Date(Fechaentraga.getTime()));
        return (long) q.executeUnique();
    }
    public long eliminarProveedorr (PersistenceManager pm, long ProveedorID)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaProveedor() + "\" WHERE \"ProveedorID\" = ?");
        q.setParameters(ProveedorID);
        return (long) q.executeUnique();
    }
    public List<Proveedor> listar(PersistenceManager pm)
    {

        Query q = pm.newQuery(SQL, "SELECT * FROM \"" + pp.darTablaProveedor()+"\"");
        q.setResultClass(Proveedor.class);
        return (List<Proveedor>) q.executeList();
    }

}
