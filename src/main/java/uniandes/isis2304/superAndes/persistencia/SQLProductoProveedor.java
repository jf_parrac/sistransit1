package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLProductoProveedor {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;
    public SQLProductoProveedor(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }


    public long adicionarProductoProveedor(PersistenceManager pm, long PPID, String Precioventassupermercado)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaProductoProveedor() + "\"(\"ProoductoproevedorID\", \"precioventasupermercado\") values (?, ?)");
        q.setParameters(PPID,Precioventassupermercado);
        return (long) q.executeUnique();
    }
    public long eliminarProductoProveedor (PersistenceManager pm, long idPP)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaProductoProveedor() + "\" WHERE \"ProoductoproevedorID\" = ?");
        q.setParameters(idPP);
        return (long) q.executeUnique();
    }
}
