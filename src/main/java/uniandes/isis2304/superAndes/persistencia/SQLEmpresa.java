package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLEmpresa {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLEmpresa(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }


    public long adicionarEmpresa (PersistenceManager pm, long EmpresaID,String Direccion,String NIT)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaEmpresa () + "\" (\"EmpresaID\", \"Direccion\",\" Nit\") values (?, ?, ?)");
        q.setParameters(EmpresaID ,Direccion,NIT);
        return (long)q.executeUnique();
    }


    public long eliminarEmpresa (PersistenceManager pm, long EmpresaID)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaEmpresa () + "\" WHERE \"EmpresaID\" = ?");
        q.setParameters(EmpresaID);
        return (long) q.executeUnique();
    }
}
