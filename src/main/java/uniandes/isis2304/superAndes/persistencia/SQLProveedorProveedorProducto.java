package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLProveedorProveedorProducto {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;
    public SQLProveedorProveedorProducto(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }

    public long adicionarPPP(PersistenceManager pm, long ProductoProveedorID, long proveedorID)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaSuperAndes() + "\" (\"ProductoproveedorID\" ,\"ProveedorID\" ) values ( ? ,? )");
        q.setParameters(ProductoProveedorID,proveedorID);
        return (long) q.executeUnique();
    }
    public long eliminarPPP(PersistenceManager pm, long ProductoProveedorID, long proveedorID)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaSuperAndes() + "\" WHERE \"SuperandesID\" = ? AND \"ProveedorID\"");
        q.setParameters(ProductoProveedorID,proveedorID);
        return (long) q.executeUnique();
    }
}
