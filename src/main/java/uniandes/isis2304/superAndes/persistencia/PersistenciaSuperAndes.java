package uniandes.isis2304.superAndes.persistencia;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import org.apache.log4j.Logger;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import uniandes.isis2304.superAndes.negocio.*;


public class PersistenciaSuperAndes {

    //CONSTANTES
    private static  Logger  log = Logger.getLogger(PersistenciaSuperAndes.class.getName());

    public static  final String SQL = "javax.jdo.query.SQL";

    // Atributos--

    private static  PersistenciaSuperAndes instance;

    private PersistenceManagerFactory pmf;

    private List<String> tablas;

    private SQLUtil sqlUtil;



    private SQLBodega sqlBodega;
    private SQLCliente sqlCliente;
    private SQLEmpresa sqlEmpresa;
    private SQLEstante sqlEstante;
    private SQLPersonaNatural sqlPersonaNatural;
    private SQLProducto sqlProducto;
    private SQLProductoProveedor sqlProductoProveedor;
    private SQLProductoSucursal sqlProductoSucursal;
    private SQLProveedor sqlProveedor;
    private SQLSucursal sqlSucursal;
    private SQLSuperAndes sqlSuperAndes;
    private SQLPromociones sqlPromociones;
    private SQLVentas sqlVentas;

    private PersistenciaSuperAndes()
    {
        pmf = JDOHelper.getPersistenceManagerFactory("SuperAndes");
        crearClasesSQL();
        tablas = new ArrayList<>();

        tablas.add("SUPERANDES_SEQUENCE");
        tablas.add("BODEGA");
        tablas.add("CLIENTE");
        tablas.add("CLIENTESUPERANDES");
        tablas.add("EMPRESA");
        tablas.add("ESTANTE");
        tablas.add("PERSONANATURAL");
        tablas.add("PRODUCTOSUCURSAL");
        tablas.add("PRODUCTO");
        tablas.add("PRODUCTOPROEVEEDOR");
        tablas.add("PROMOCIONES");
        tablas.add("PROVEEDOR");
        tablas.add("PROVEEDORPRODUCTOPROVEEDOR");
        tablas.add("SUCURSAL");
        tablas.add("SUPERANDES");
        tablas.add("VENTAS");

    }
    private  PersistenciaSuperAndes(JsonObject tableConfig)
    {
        crearClasesSQL();
        tablas= leerNombresTablas(tableConfig);

        String unidadPersistencia = tableConfig.get ("unidadPersistencia").getAsString ();
        log.trace ("Accediendo unidad de persistencia: " + unidadPersistencia);
        pmf = JDOHelper.getPersistenceManagerFactory (unidadPersistencia);
    }
    public static PersistenciaSuperAndes getInstance ()
    {
        if (instance == null) {
            instance = new PersistenciaSuperAndes();
        }
        return instance;
    }

    /**
     * Constructor que toma los nombres de las tablas de la base de datos del objeto tableConfig
     * @param tableConfig - El objeto JSON con los nombres de las tablas
     * @return Retorna el único objeto PersistenciaParranderos existente - Patrón SINGLETON
     */
    public static PersistenciaSuperAndes getInstance (JsonObject tableConfig)
    {
        if (instance == null)
        {
            instance = new PersistenciaSuperAndes (tableConfig);
        }
        return instance;
    }

    /**
     * Cierra la conexión con la base de datos
     */
    public void cerrarUnidadPersistencia ()
    {
        pmf.close ();
        instance = null;
    }


    /**
     * Genera una lista con los nombres de las tablas de la base de datos
     * @param tableConfig - El objeto Json con los nombres de las tablas
     * @return La lista con los nombres del secuenciador y de las tablas
     */
    private List <String> leerNombresTablas (JsonObject tableConfig)
    {
        JsonArray nombres = tableConfig.getAsJsonArray("tablas") ;

        List <String> resp = new LinkedList <String> ();
        for (JsonElement nom : nombres)
        {
            resp.add (nom.getAsString ());
        }

        return resp;
    }
    private void crearClasesSQL ()
    {
        sqlBodega= new SQLBodega(this);
        sqlCliente = new SQLCliente(this);
        sqlEmpresa= new SQLEmpresa((this));
        sqlEstante = new SQLEstante(this);
        sqlPersonaNatural = new SQLPersonaNatural(this);
        sqlProducto = new SQLProducto(this);
        sqlProductoProveedor = new SQLProductoProveedor(this);
        sqlProductoSucursal = new SQLProductoSucursal(this);
        sqlProveedor= new SQLProveedor(this);
        sqlSucursal= new SQLSucursal(this);
        sqlSuperAndes= new SQLSuperAndes(this);
        sqlPromociones=new SQLPromociones(this);
        sqlVentas= new SQLVentas(this);
        sqlUtil = new SQLUtil(this);

    }

    /**
     * Transacción para el generador de secuencia de Parranderos
     * Adiciona entradas al log de la aplicación
     * @return El siguiente número del secuenciador de Parranderos
     */
    private long nextval ()
    {
        long resp = sqlUtil.nextval (pmf.getPersistenceManager());
        log.trace ("Generando secuencia: " + resp);
        return resp;
    }


    /**
     * Extrae el mensaje de la exception JDODataStoreException embebido en la Exception e, que da el detalle específico del problema encontrado
     * @param e - La excepción que ocurrio
     * @return El mensaje de la excepción JDO
     */
    private String darDetalleException(Exception e)
    {
        String resp = "";
        if (e.getClass().getName().equals("javax.jdo.JDODataStoreException"))
        {
            JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
            return je.getNestedExceptions() [0].getMessage();
        }
        return resp;
    }
        /*tablas.add("SUPERANDES_SEQUENCE");
        tablas.add("BODEGA");
        tablas.add("CLIENTE");
        tablas.add("CLIENTESUPERANDES");
        tablas.add("EMPRESA");
        tablas.add("ESTANTE");
        tablas.add("PERSONANATURAL");
        tablas.add("PRODUCTOSUCURSAL");
        tablas.add("PRODUCTO");
        tablas.add("PRODUCTOPROEVEEDOR");
        tablas.add("PROMOCIONES");
        tablas.add("PROVEEDOR");
        tablas.add("PROVEEDORPRODUCTOPROVEEDOR");
        tablas.add("SUCURSAL");
        tablas.add("SUPERANDES");
        tablas.add("VENTAS");*/

    public String darSeqSuperAndes()            {return "SUPERANDES_SEQUENCE";}
    public String darTablaBodega()              {return "Bodega";}
    public String darTablaCliente()             {return "Cliente";}
    public String darTablaClienteSuperAndes()   {return "ClienteSuperAndes";}
    public String darTablaEmpresa()             {return "Empresa";}
    public String darTablaEstante()             {return "Estante";}
    public String darTablaPersonaNatural()      {return "Personanatural";}
    public String darTablaProductoSucursal()    {return "Productosucursal";}
    public String darTablaProducto()            {return "Producto";}
    public String darTablaProductoProveedor()  {return "Productoproeveedor";}
    public String darTablaPromociones()        {return "PROMOCIONES";}
    public String darTablaProveedor()          {return "Proveedor";}
    public String darTablaProveedorProveedorProducto(){return "ProveedorProductoProeveedor";}
    public String darTablaSucursal()                  {return "Sucursal";}
    public String darTablaSuperAndes()                {return "Superandes";}
    public String darTablaVentas()                    {return "VENTAS";}


    public SuperAndes adicionarSupermercado(String nombre)
    {
      PersistenceManager pm=pmf.getPersistenceManager();
      Transaction tx=pm.currentTransaction();
      try
      {
          tx.begin();
          long idSupermercado=nextval();
          long tuplasinsertadas=sqlSuperAndes.adicionarSupermercado(pm,idSupermercado,nombre);
          tx.commit();

          log.trace("Insercion de Supermercado_ "+nombre+": "+tuplasinsertadas+" tuplas insertadas");
          SuperAndes rta= new SuperAndes();
          rta.setNombre(nombre);
          rta.setSuperandesID(idSupermercado);
          return rta;

      }
      catch (Exception e)
      {e.printStackTrace();
        log.error("Exception: "+e.getMessage()+"\n"+darDetalleException(e));
        return null;
      }
      finally {
          if(tx.isActive())
          {
              tx.rollback();
          }
          pm.close();
      }


    }

    public Proveedor adicionarProveedor(String nombre, String nit, Date fechaentrega, long idS)
    {
        PersistenceManager pm=pmf.getPersistenceManager();
        Transaction tx=pm.currentTransaction();
        try
        {
            tx.begin();
            long id=nextval();
            long tuplasinsertadas=sqlProveedor.adicionarProveedor(pm,id,fechaentrega,nit,nombre,idS);
            tx.commit();

            log.trace("Insercion de Supermercado_ "+nombre+": "+tuplasinsertadas+" tuplas insertadas");
            Proveedor rta= new Proveedor();
            rta.setNombre(nombre);
            rta.setNit(nit);
            rta.setFechaentraga(fechaentrega);
            rta.setProveedorID(id);
            return rta;

        }
        catch (Exception e)
        {e.printStackTrace();
            log.error("Exception: "+e.getMessage()+"\n"+darDetalleException(e));
            return null;
        }
        finally {
            if(tx.isActive())
            {
                tx.rollback();
            }
            pm.close();
        }

    }

    public Bodega adicionarBodega(long sucursalid, double vol, double peso, double capPeso, double capVol, String nivel, String tipo){
        PersistenceManager pm= pmf.getPersistenceManager();
        Transaction t=pm.currentTransaction();
        try{
            t.begin();
            long idBodega=nextval();
            eCategoria c=eCategoria.OTROS;
            for (eCategoria a:
                    eCategoria.values()) {
                if(tipo.equals(a.name())) c=a;
            }
            long tuplasInsertadas=sqlBodega.adicionarBodega(pm,idBodega,capPeso,vol,nivel,peso,c,capVol,sucursalid);
            t.commit();
            log.trace("Insercion de bodega: "+ tipo+" : "+ tuplasInsertadas+" tuplas insertadas");
            return new Bodega(capPeso,capVol,nivel,peso,tipo,vol,idBodega);
        }catch (Exception e){
            log.error("Exception : "+e.getMessage()+ "\n"+darDetalleException(e));
            return null;
        }finally {
            if(t.isActive()){
                t.rollback();
            }
            pm.close();
        }
    }

    public Producto adicionarProducto(float cantidad,String codigoBarras,Date vencimiento,String marca,String nombre,float pesoEmpaque,String presentacion,float preciounidaddemedida,float preciounitario,String tipoproducto,String unidadedida,float volumenempaque,float preciocompra, float precioventa,long sucursalid)
    {
        PersistenceManager pm= pmf.getPersistenceManager();
        Transaction t=pm.currentTransaction();

        try{
            t.begin();
            long idProducto=nextval();

            long tuplasInsertadas=sqlProducto.adicionarProducto(pm,cantidad,codigoBarras,vencimiento,marca,nombre,pesoEmpaque,presentacion,preciounidaddemedida,preciounitario,tipoproducto,unidadedida,volumenempaque);




            t.commit();
            return null;
        }catch (Exception e){
            log.error("Exception : "+e.getMessage()+ "\n"+darDetalleException(e));
            return null;
        }finally {
            if(t.isActive()){
                t.rollback();
            }
            pm.close();
        }

    }

    public List<SuperAndes> listarSupermercados()
    {
        return sqlSuperAndes.listar(pmf.getPersistenceManager());
    }
    public List<Proveedor> listarProveedores()
    {
        return  sqlProveedor.listar(pmf.getPersistenceManager());
    }
    public List<Bodega> listarBodegas()
    {
        return  sqlBodega.listar(pmf.getPersistenceManager());
    }
    public List<Estante> listarEstantes()
    {
        return  sqlEstante.listar(pmf.getPersistenceManager());
    }

}
