package uniandes.isis2304.superAndes.persistencia;


import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLPersonaNatural {
    private final static String SQL = PersistenciaSuperAndes.SQL;
    private PersistenciaSuperAndes pp;

    public SQLPersonaNatural(PersistenciaSuperAndes pp)
    {
        this.pp=pp;
    }


    public long adicionarEstante(PersistenceManager pm, long PersonaID, String Documentoidentificacion)
    {
        Query q = pm.newQuery(SQL, "INSERT INTO \"" + pp.darTablaPersonaNatural () + "\" (\"ID\", \"Documentoidentificacion\") values (?, ?, ?)");
        q.setParameters(PersonaID,Documentoidentificacion);
        return (long) q.executeUnique();
    }
    public long eliminarEstante (PersistenceManager pm, long idPersona)
    {
        Query q = pm.newQuery(SQL, "DELETE FROM \"" + pp.darTablaPersonaNatural () + "\" WHERE \"ID\" = ?");
        q.setParameters(idPersona);
        return (long) q.executeUnique();
    }

}
