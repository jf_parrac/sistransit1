/* ---------------------------------------------------- */
/*  Created On : 30-sept.-2018 12:25:25 a. m. 				*/
/*  DBMS       : Oracle 						*/
/* ---------------------------------------------------- */

create sequence superAndes_sequence;
/* Create Tables */

CREATE TABLE "Bodega"
(
    "Capacidadpeso" FLOAT,
    "Capacidadvolumen" FLOAT,
    "Nivelreorden" VARCHAR2(20),
    "Pesodisponible" FLOAT,
    "Tipoalmacenamiento" VARCHAR2(20) DEFAULT 'OTROS',
    "Volumendisponible" FLOAT,
    "BodegaID" NUMBER NOT NULL,
    "SucursalID" NUMBER
)
;


CREATE TABLE  "Cliente"
(
	"Email" VARCHAR2(50),
	"Nombre" VARCHAR2(50),
	"ClienteID" NUMBER NOT NULL
)
;

CREATE TABLE  "ClienteSuperAndes"
(
	"supermercado" NUMBER,
	"cliente" NUMBER
)
;

CREATE TABLE  "Empresa"
(
	"Direccion" VARCHAR2(20),
	"Nit" VARCHAR2(20),
	"EmpresaID" NUMBER NOT NULL
)
;

CREATE TABLE "Estante"
(
    "Nivelavastecimiento" FLOAT,
    "Tipoestante" VARCHAR2(20) DEFAULT 'OTROS',
    "EstanteID" NUMBER NOT NULL,
    "SucursalID" NUMBER
)
;

CREATE TABLE  "Personanatural"
(
	"Documentoidentificaion" VARCHAR2(20),
	"ID" NUMBER NOT NULL
)
;

CREATE TABLE "Producosucursal"
(
    "Precioventapublico" FLOAT,
    "ProducosucursalID" NUMBER NOT NULL,
    "BodegaID" NUMBER,
    "EstanteID" NUMBER
)
;

CREATE TABLE  "Producto"
(
	"Cantidadproducto" NUMBER(8,2),
	"Codigodebarras" VARCHAR2(20),
	"Fechavencimiento" DATE,
	"Marca" VARCHAR2(20),
	"Nombre" VARCHAR2(50),
	"Pesoempaque" FLOAT,
	"Precentacion" VARCHAR2(20),
	"Precioporunidaddemedida" FLOAT,
	"Preciounitario" FLOAT,
	"Tipoproducto" VARCHAR2(20),
	"Unidadmedida" VARCHAR(10),
	"Volumenempaque" FLOAT(8),
	"ProductoID" NUMBER NOT NULL
)
;

CREATE TABLE  "Productoproeveedor"
(
	"Precioventasupermercado" FLOAT,
	"ProductoproeveedorID" NUMBER NOT NULL
)
;

CREATE TABLE  "Proveedor"
(
	"Calificaciodecalidad" VARCHAR2(20),
	"Fechaentraga" DATE,
	"Nit" VARCHAR2(20),
	"Nombre" VARCHAR2(50),
	"ProveedorID" NUMBER NOT NULL,
	"SuperandesID" NUMBER
)
;

CREATE TABLE  "ProveedorProductoProeveedor"
(
	"ProductoproeveedorID" NUMBER,
	"ProveedorID" NUMBER
)
;



CREATE TABLE "Sucursal"
(
    "Ciudad" varchar(20),
    "Direccion" varchar(20),
    "Segmernomercado" varchar(20),
    "SucursalID" NUMBER NOT NULL,
    "SuperMercado" NUMBER
)
;

CREATE TABLE  "Superandes"
(
	"Nombre" VARCHAR2(50),
	"SuperandesID" NUMBER NOT NULL
)
;



/* Create Primary Keys, Indexes, Uniques, Checks, Triggers */

ALTER TABLE  "Bodega"
	ADD CONSTRAINT "PK_Bodega"
PRIMARY KEY ("BodegaID")
	USING INDEX
;

ALTER TABLE  "Cliente"
	ADD CONSTRAINT "PK_Cliente"
PRIMARY KEY ("ClienteID")
	USING INDEX
;

ALTER TABLE  "Empresa"
	ADD CONSTRAINT "PK_Empresa"
PRIMARY KEY ("EmpresaID")
	USING INDEX
;

ALTER TABLE  "Estante"
	ADD CONSTRAINT "PK_Estante"
PRIMARY KEY ("EstanteID")
	USING INDEX
;

ALTER TABLE  "Personanatural"
	ADD CONSTRAINT "PK_Personanatural"
PRIMARY KEY ("ID")
	USING INDEX
;

ALTER TABLE  "Producosucursal"
	ADD CONSTRAINT "PK_Producosucursal"
PRIMARY KEY ("ProducosucursalID")
	USING INDEX
;

ALTER TABLE  "Producto"
	ADD CONSTRAINT "PK_Producto"
PRIMARY KEY ("ProductoID")
	USING INDEX
;

ALTER TABLE  "Productoproeveedor"
	ADD CONSTRAINT "PK_Productoproeveedor"
PRIMARY KEY ("ProductoproeveedorID")
	USING INDEX
;

ALTER TABLE  "Proveedor"
	ADD CONSTRAINT "PK_Proveedor"
PRIMARY KEY ("ProveedorID")
	USING INDEX
;

ALTER TABLE  "Sucursal"
	ADD CONSTRAINT "PK_Sucursal"
PRIMARY KEY ("SucursalID")
	USING INDEX
;

ALTER TABLE  "Superandes"
	ADD CONSTRAINT "PK_Superandes"
PRIMARY KEY ("SuperandesID")
	USING INDEX
;

/* Create Foreign Key Constraints */

ALTER TABLE  "Bodega"
	ADD CONSTRAINT "FK_Bodega_bodegas"
FOREIGN KEY ("SucursalID") REFERENCES  "Sucursal" ("SucursalID")
;

ALTER TABLE  "ClienteSuperAndes"
	ADD CONSTRAINT "FK_CSuperAndes_supermercado"
FOREIGN KEY ("supermercado") REFERENCES  "Superandes" ("SuperandesID")
;

ALTER TABLE  "ClienteSuperAndes"
	ADD CONSTRAINT "FK_CSuperAndes_cliente"
FOREIGN KEY ("cliente") REFERENCES  "Cliente" ("ClienteID")
;

ALTER TABLE  "Empresa"
	ADD CONSTRAINT "FK_Empresa_Cliente"
FOREIGN KEY ("EmpresaID") REFERENCES  "Cliente" ("ClienteID")
;

ALTER TABLE  "Estante"
	ADD CONSTRAINT "FK_Estante_estantes"
FOREIGN KEY ("SucursalID") REFERENCES  "Sucursal" ("SucursalID")
;

ALTER TABLE  "Personanatural"
	ADD CONSTRAINT "FK_PeNatural_Cliente"
FOREIGN KEY ("ID") REFERENCES  "Cliente" ("ClienteID")
;

ALTER TABLE  "Producosucursal"
	ADD CONSTRAINT "FK_PSucursal_pAlmacenados"
FOREIGN KEY ("BodegaID") REFERENCES  "Bodega" ("BodegaID")
;

ALTER TABLE  "Producosucursal"
	ADD CONSTRAINT "FK_PSucursal_pEnVenta"
FOREIGN KEY ("EstanteID") REFERENCES  "Estante" ("EstanteID")
;

ALTER TABLE  "Producosucursal"
	ADD CONSTRAINT "FK_PSucursal_Producto"
FOREIGN KEY ("ProducosucursalID") REFERENCES  "Producto" ("ProductoID")
;

ALTER TABLE  "Productoproeveedor"
	ADD CONSTRAINT "FK_PP_Producto"
FOREIGN KEY ("ProductoproeveedorID") REFERENCES  "Producto" ("ProductoID")
;

ALTER TABLE  "Proveedor"
	ADD CONSTRAINT "FK_P_proveedores"
FOREIGN KEY ("SuperandesID") REFERENCES  "Superandes" ("SuperandesID")
;

ALTER TABLE  "ProveedorProductoProeveedor"
	ADD CONSTRAINT "FK_PPProeveedor_PProeveedor"
FOREIGN KEY ("ProductoproeveedorID") REFERENCES  "Productoproeveedor" ("ProductoproeveedorID")
;

ALTER TABLE  "ProveedorProductoProeveedor"
	ADD CONSTRAINT "FK_PPP_Proveedor"
FOREIGN KEY ("ProveedorID") REFERENCES  "Proveedor" ("ProveedorID")
;

ALTER TABLE  "Sucursal"
	ADD CONSTRAINT "FK_Sucursal_SA"
FOREIGN KEY ("SuperMercado") REFERENCES  "Superandes" ("SuperandesID")
;

ALTER TABLE "Cliente"
	ADD CONSTRAINT CK_email
CHECK ("Email" LIKE ('%@%.%'))
;

ALTER TABLE "Bodega"
  ADD CONSTRAINT CK_TipoAlmacenamiento
CHECK ("Tipoalmacenamiento" IN ('PERECEDEROS', 'NO PERECEDEROS', 'ASEO','CONGELADOS','ROPA','MUEBLES','HERRAMIENTAS','ELECTRODOMESTICOS','OTROS'))
ENABLE;

ALTER TABLE "Estante"
  ADD CONSTRAINT CK_TipoEstante
CHECK ("Tipoestante" IN ('PERECEDEROS', 'NO PERECEDEROS', 'ASEO','CONGELADOS','ROPA','MUEBLES','HERRAMIENTAS','ELECTRODOMESTICOS','OTROS'))
ENABLE;

ALTER  TABLE  "Producto"
  ADD CONSTRAINT  CK_tipoProducto
CHECK( "Tipoproducto" IN ('PERECEDEROS', 'NO PERECEDEROS', 'ASEO','CONGELADOS','ROPA','MUEBLES','HERRAMIENTAS','ELECTRODOMESTICOS','OTROS'))
ENABLE;


CREATE TABLE "PROMOCIONES"
(
  PromocionID NUMBER PRIMARY KEY,
  ProductoPromocion NUMBER,
  TipoPromocion NUMBER,
  DescripcionPromocion VARCHAR2(50),
  FechaInicio DATE,
  FechaFin DATE,
  CONSTRAINT FK_Producosucursal FOREIGN KEY (ProductoPromocion) REFERENCES "Producosucursal" ("ProducosucursalID")
);


ALTER TABLE "PROMOCIONES"
  ADD CONSTRAINT CK_TipoPromocion
CHECK (TipoPromocion between 1 AND 5)
  ADD CONSTRAINT CK_dates
CHECK (FechaFin>FechaInicio)
ENABLE;

CREATE TABLE "VENTAS"
(
  producto NUMBER,
  cantidad NUMBER,
  puntoVenta Number,
  promocion Number,
  Cliente NUMBER,
  VentaID NUMBER PRIMARY KEY,
  CONSTRAINT VENTAS_CLIENTE_fk FOREIGN KEY (Cliente) REFERENCES "Cliente" ("ClienteID"),
  CONSTRAINT VENTAS_SUCURSAL_fk FOREIGN KEY (puntoVenta) REFERENCES "Sucursal" ("SucursalID"),
  CONSTRAINT VENTAS_PRODUCTO_fk FOREIGN KEY (producto) REFERENCES "Producosucursal" ("ProducosucursalID"),
  CONSTRAINT VENTAS_PROMOCION_fk FOREIGN KEY (promocion) REFERENCES "PROMOCIONES" (PromocionID)
);
